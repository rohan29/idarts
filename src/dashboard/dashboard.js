import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { dataApiStart } from "../redux/reducers/dashboard-reducer/dashboard-reducer";
import loader from "../images/loader.gif";
import FirstCol from "../components/Cards/firstCol";
import SecondCol from "../components/Cards/secondCol";
import MarketValue from "../components/Cards/marketValue";
import UnrealizedPL from "../components/Cards/unrealizedPL";
import {
    PieChart,
    Pie,
    Cell,
    ResponsiveContainer,
    Legend,
} from "recharts";

import "../components/Cards/style.css";

export default function Dashboard() {
    const piedata = [
        { name: "Mutual Funds", value: 5 },
        { name: "ETFs", value: 5 },
    ];
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch({ type: dataApiStart.type, payload: "" });
    }, []);
    const data = useSelector((state) => state.dashboardData.data);
    if (data.length === 0) {
        return <div className="loader-container">
            <img src={loader} alt="loader" />
        </div>
    }
    return (
        <div className="row">
            <div className="col-md-9">
                {data &&
                    data.map((data) => (
                        <div className="d-flex tile-row">
                            <FirstCol data={data} />
                            <SecondCol data={data} />
                            <MarketValue data={data} />
                            <UnrealizedPL data={data} />
                            <div className="button-container">
                                <button className="cursor">BUY</button>
                                <button className="cursor">SELL</button>
                            </div>
                        </div>
                    ))}
            </div>
            <div className="col-md-3">
                <div className="chart-container">
                    <div className="chart-wrapper">
                        <div className="d-flex space-between">
                            <span className="weight-700">Portfolio</span>
                            <select>
                                <option>Asset wise</option>
                            </select>
                        </div>
                        <ResponsiveContainer width="100%" height={250}>
                            <PieChart height={250}>
                                <Pie
                                    data={piedata}
                                    cx="50%"
                                    cy="50%"
                                    outerRadius={80}
                                    innerRadius={60}
                                    fill="#8884d8"
                                    dataKey="value"
                                >
                                    {piedata.map((entry, index) => (
                                        <>
                                            <Cell fill="#03a9f4" />
                                            <Cell fill="#ae9c46" />
                                        </>
                                    ))}
                                </Pie>
                                <Legend layout="vertical" verticalAlign="middle" align="right" />
                            </PieChart>
                        </ResponsiveContainer>
                    </div>
                </div>
            </div>
        </div>
    );
}

import Axios from "axios";

export const GET = async (url, params) => {
  const result = await Axios.get(`${url}${params}`).catch((err) => {
    console.log(err.response);
  });

  return result.data;
};

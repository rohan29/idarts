import React from "react";

import "./style.css";
export default function UnrealizedPL(props) {
  const { data } = props;

  const persentageReturn = (unrealized, InvestedAmount) => {
    let perreturn = (unrealized * 100) / InvestedAmount;
    return perreturn.toFixed(2);
  };

  return (
    <div className="tile urealized-value-tile">
      <div className="d-flex space-between">
        <span className="font-16 weight-700">Unrealized P/L</span>
        <span className="font-16 weight-700">{data.unrealizedPL}</span>
      </div>
      <div className="d-flex space-between">
        <span className="color-lightgray">% Return</span>
        <span className="font-16 weight-700">
          {persentageReturn(data.unrealizedPL, data.invested_Amount) < 0 ? (
            <span className="icons" style={{ color: "red" }}>
              <i class="fas fa-caret-down"></i>
            </span>
          ) : (
            <span className="icons" style={{ color: "green" }}>
              <i class="fas fa-caret-up"></i>
            </span>
          )}
          {persentageReturn(data.unrealizedPL, data.invested_Amount)}%
        </span>
      </div>

      <div className="d-flex progress-container">
        <progress
          className="reverse-progress"
          value={
            persentageReturn(data.unrealizedPL, data.invested_Amount) < 0
              ? Math.abs(persentageReturn(data.unrealizedPL, data.invested_Amount))
              : "0"
          }
          color="green"
          max="100"
        ></progress>
        <progress
          value={
            persentageReturn(data.unrealizedPL, data.invested_Amount) > 1
              ? persentageReturn(data.unrealizedPL, data.invested_Amount)
              : "0"
          }
          color="green"
          max="100"
        ></progress>
      </div>
    </div>
  );
}

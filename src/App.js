import "./App.css";
import { Provider } from "react-redux";
import Dashboard from "./dashboard/dashboard";
import Store from "./redux/store/store";
import logo from "./images/logo.jpg";

function App() {

  return (
    <Provider store={Store}>
      <div className="App">
        <div>
          <header>
            <img src={logo} alt="logo" className="header-logo" />
          </header>
        </div>
        <Dashboard />
      </div>
    </Provider>
  );
}

export default App;

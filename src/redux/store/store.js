
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { rootReducer } from "../reducers/rootReducer";
import { rootSaga } from "../sagas/rootSaga";



const sagaMiddleware = createSagaMiddleware();
const middleware = [...getDefaultMiddleware({ thunk: false }), sagaMiddleware];

const Store = configureStore({ reducer: rootReducer, middleware });

export default Store;

sagaMiddleware.run(rootSaga);

import { put, call, takeLatest } from "redux-saga/effects";
import { GET } from "../../../components/FetchMethod/FetchMethod";
import { dashboard_url } from "../../../config/urls";

import {
  getDataSuccess,
  dataApiStart,
} from "../../reducers/dashboard-reducer/dashboard-reducer";

function* getData(action) {
  try {
    let params = action.payload;
    let result = yield call(GET, dashboard_url, params);
    yield put(getDataSuccess(result));
  } catch {}
}

export function* dashboardSaga() {
  yield takeLatest(dataApiStart.type, getData);
}

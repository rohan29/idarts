import { combineReducers } from "redux";

import dashboardData from "./dashboard-reducer/dashboard-reducer"

export const rootReducer = combineReducers({
    dashboardData,
});

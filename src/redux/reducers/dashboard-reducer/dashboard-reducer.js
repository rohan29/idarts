import { createSlice, createAction } from "@reduxjs/toolkit";

const dashboardSlice = createSlice({
  name: "dashboard",
  initialState: {
    data: [],
  },
  reducers: {
    getDataSuccess: (state, action) => {
      state.data = action.payload;
    },
  },
});

const { actions, reducer } = dashboardSlice;

export const dataApiStart = createAction("dataApiStart");

export const { getDataSuccess } = actions;

export default reducer;
